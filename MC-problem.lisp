;Solve the missionaries and cannibals problem
;Author： Shiqiang Xia
;        2019-12-01, Minneapolis
;-------------------------PROBLEM-----------------------------------------------
;Problem: Give M0 missionaries, C0 cannibals and one boat with capacity B0
;Goal: Move all missionaries and cannibals from one side of the river to the other side
;Constraints: On each side and on the boat, if M ~=0 , M>=C
;-------------------------METHOD------------------------------------------------
;Solve the problem by Depth frist Search
;State space: (M, C, B)
;M & C :number of missionaries and cannibals on the left side(beginning side)
;B: L(left side),R(right side)
;operate: (m,c) e.g (6,0), (4,2), -(4,2) (positive means from R to L, negative means from L to R)
;heuristic: h = (M+C)/B0
;start state: (M0, C0, L)
;Goal state: (0,0,R)
;-------------------------STEPS-------------------------------------------------
;1. Initial the problem with the initial state (M0, C0, L)
;2. For a given state, call the function MC-Solver
;3. For this state, expand all possible states that satisfy the constraint and are not visited.
;4. Check if there is a goal state in the possible states and add all the possible states to a visited list.
;5. If yes, stop and return the goal state.
;6. If no, for each possible state, call function MC-Solver recursively till return.
;7. If return NIL, there is no solution. If return the goal state, build the path and operators from the initial state to the goal state and print the path. 

;-------------------------------------------------------------------------------

(defstruct state
  node
  parent
)

;-------------------------------------------------------------------------------
(defun MC-Driver(num_missionary num_cannibal cap_boad)
(setf (values m0 c0 b0) (values num_missionary num_cannibal cap_boad))
(format t "-------------------------------------~%")
(format t "Missionaries and Cannibals problem~%" )
(format t "M=~A, C=~A, One boad with capacity=~A~%" m0 c0 b0)
(format t "Solving the problem.......~%")

(setf istate (cons m0 (cons c0 (cons 'L nil))))
(setf initState (make-state
  :node istate
  :parent nil))
(setf operators (BuildOperator b0))
(setf VisitedNodes (cons istate nil))
(setf (values goalState VisitedNodes) (MC-Solver initState operators m0 c0 b0 VisitedNodes))
(format t "Finished!~%")
(setf (values path opts) (BuildPath goalState))
(MC-Printer paths opts)
)
;-------------------------------------------------------------------------------

(defun BuildOperator(b0)
"build a list of possible operators; assume possible here"
(setf opts nil)

(do ((m 1 (1+ m)))
    ((> m b0) 'done)
    (do ((c 0 (1+ c)))
    ((> c (min m (- b0 m))) 'done)
    (setf opts (cons (cons m (cons c nil)) opts))
    )
  )
(do ((cc 1 (1+ cc)))
    ((> cc b0) 'done)
    (setf opts (cons  (cons 0 (cons cc nil)) opts))
)
opts

)
;-------------------------------------------------------------------------------

(defun MC-Solver(mystate operators m0 c0 b0 VisitedNodes)
"For a given sate and operators, recursively the goal state"
(setf node (state-node mystate))
;(setf hvalue (Heuristic node b0))
(setf PossibleNodes (BuildNodes node operators m0 c0))
(setf PossibleNodes (RemoveVisited PossibleNodes VisitedNodes))
(cond
  ((equal PossibleNodes nil) (setf (values rlt1 rlt2) (values nil VisitedNodes)))
  (t
    (setf VisitedNodes (append VisitedNodes PossibleNodes))
    (setf PendingStates nil)
    (setf flag nil)
    (loop for tempnode in PossibleNodes
      do(setf hvalue (Heuristic tempnode b0))
        (setf tempstate (make-state :node tempnode :parent mystate))
        (setf PendingStates (cons tempstate PendingStates))
        (cond
          ( (equal hvalue 0)
            (setf flag t)
            (setf (values rlt1 rlt2) (values tempstate VisitedNodes))
            (return)
            )
          )
      )
    (cond
      ( (equal flag nil)
        (loop for ts in PendingStates
          do  (setf (values tempt-rlt VisitedNodes) (MC-Solver ts operators m0 c0 b0 VisitedNodes))
              (cond
                ((not (equal tempt-rlt nil))
                (setf flag t)
                (setf (values rlt1 rlt2) (values tempt-rlt VisitedNodes))
                (return)
                  )
                )
          )

        )
      )
      (cond
        ((equal flag nil) (setf (values rlt1 rlt2) (values nil VisitedNodes)))
        )

    )
  )

(values rlt1 rlt2)

)
;-------------------------------------------------------------------------------
(defun CheckSafty(state m0 c0)
;Check valid
(defun IsSafe(mm cc)
(cond
  ((or (< mm 0) (< cc 0)) nil)
  ((and (> mm 0) (< mm cc)) nil)
  ((and (> mm 0) (>= mm cc)) t)
  ((equal mm 0) t)
)
)

(setf m (nth 0 state))
(setf c (nth 1 state))

(setf r1 (IsSafe m c)) ; Test one side
(setf r2 (IsSafe (- m0 m) (- c0 c))) ;Test the other side
(and r1 r2)
)
;-------------------------------------------------------------------------------

(defun Heuristic(state b0)
; I actually did't use heuristic.
;I just use it to check if this is the goal state.
(setf rlt (/ (+ (nth 0 state) (nth 1 state)) b0))
)
;-------------------------------------------------------------------------------
(defun RemoveVisited(cands recors)
(setf rlt nil)
;(setf flag t)
(loop for candidate in cands
  do (setf flag t)
    (loop for rec in recors
      do(cond
        ((equal candidate rec) (setf flag nil))
        )
    )
    ;(format t "~A ~A ~%" candidate flag)
    (cond
      (flag (setf rlt (cons candidate rlt)))
      )
  )
rlt
)
;-------------------------------------------------------------------------------
(defun BuildNodes(state operators m0 c0)
(setf nds nil)

(setf (values mi ci bi) (values (nth 0 state) (nth 1 state) (nth 2 state)))
(cond
  ((equal bi 'L) (setf (values flag bp) (values -1 'R)))
  ((equal bi 'R) (setf (values flag bp) (values 1 'L)))
  )

;(format t "~A ~A ~%" flag bp)

(loop for opt in operators
  do (setf (values ms cs) (values (nth 0 opt) (nth 1 opt)))

      (setf (values ms cs) (values (* ms flag) (* cs flag)))

      (setf (values mn cn) (values (+ mi ms) (+ ci cs)))
      ;(format t "~A ~A ~A ~%" opt mn cn)

      (setf tempstate (cons mn (cons cn (cons bp nil))))
      ;(format t "~A ~%" tempstate)
      (cond
        ((CheckSafty tempstate m0 c0) (setf nds (cons tempstate nds)))
        )
)
nds

)
;-------------------------------------------------------------------------------
;-------------------------------------------------------------------------------
(defun GetOperator(state newstate)
(setf (values mi ci bi) (values (nth 0 state) (nth 1 state) (nth 2 state)))
(setf (values mn cn bn) (values (nth 0 newstate) (nth 1 newstate) (nth 2 newstate)))
(setf (values mo co) (values (- mn mi) (- cn ci)))
(cond
  ((equal bi 'R) (setf bo 'R->L))
  (t (setf bo 'L->R))
  )
(setf rlt (cons mo (cons co (cons bo nil))))
rlt

)
;-------------------------------------------------------------------------------
(defun BuildPath(goalState)
(setf paths nil)
(setf opts nil)
(cond
  ((equal goalState nil) (format t "No solution!~%"))
  (t
    (setf temp (state-parent goalState))
    (setf node (state-node goalState))
    (setf paths (cons node paths))
    (loop while (not (equal temp nil))
          do (setf newnode (state-node temp))
             (setf myopt (GetOperator newnode node))
             (setf opts (cons myopt opts))
             (setf paths (cons newnode paths))
             (setf node newnode)
             (setf temp (state-parent temp))

      )

    )

  )
(values paths opts)

)

;-------------------------------------------------------------------------------
(defun MC-Printer(paths opts)
"Print the paths and opts alternatively"
(format t "--------Solution------------------------~%")
(format t "Print the Path~%")
     ;(format t "            (M,C,B)~%")
(setf num (length opts))
(loop for ii from 0 to (- num 1)
  do (format t "~A  " (nth ii paths))
     (format t "~A  " (nth ii opts))
     (cond
       ((equal 0 (mod (+ ii 1) 3)) (format t "~%"))
     )
  )
(format t "~A~%" (nth num paths))
(format t "-------------done!---------------------~%")
)
