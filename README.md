# Missionaries and Cannibals problem

This is the programming assignment for CSCI 5511 at the University of Minnesota. 

Author: Shiqiang Xia

Time: 11/30/2019

We implement LISP code to solve the missionaries and cannibals problem (MC problem). For M0 missionaries, C0 cannibals and one boat with capacity B0, we want to find a way to move all the missionaries and cannibals from one side of the river to the other side under the constraint that the number of missionaries, if it’s not zero, cannot be less than the number of cannibals on both sides of the river and in the boat. 

We formulate this MC problem as a search problem. Let us assume the start side of the river is L and the other side is R.

* State Space: (M, C, B), where M, C are the number of missionaries and cannibals on side L, b is where the boat is right now.
* Initial State:  (M0, M0, L)
* Goal State:   (0, 0, R)
* Operators:    (m,c), (-m,-c), where (m = 0 and c ≤ B0)  or ( m ≥ c and m+c ≤ B0) and negative numbers mean to move from L side to R side and positive numbers mean to move R side L side.

We use the depth-first search algorithm to solve this problem. Here are the steps:

 1. Initial the problem with the initial state (M0, C0, L)
2. For a given state, call the function MC-Solver
 3. For this state, expand all possible states that satisfy the constraint and are not visited. 
4. Check if there is a goal state in the possible states and add all the possible states to a visited list.
5. If yes, stop and return the goal state.
6. If no, for each possible state, call function MC-Solver recursively till return.
7. If return NIL, there is no solution. If return the goal state, build the path and operators from the initial state to the goal state and print the path. 


## How to run

`(load "MC-problem.lisp")`

`(MC-driver 21 21 6)`


